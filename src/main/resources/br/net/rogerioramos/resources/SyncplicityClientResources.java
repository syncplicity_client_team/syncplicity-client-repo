package br.net.rogerioramos.resources;

import java.io.InputStream;
import java.net.URL;

public final class SyncplicityClientResources {

	/**
	 * Get resource as Stream, the class talk about resources, so avoid to repeat the method's aim
	 * @param name
	 * @return
	 */
	public static final InputStream getStream(String name) {
		return SyncplicityClientResources.class.getResourceAsStream(name);
	}
	
	/**
	 * Get resource as URL, the class talk about resources, so avoid to repeat the method's aim
	 * @param name
	 * @return
	 */
	public static final URL get(String name) {
		return SyncplicityClientResources.class.getResource(name);
	}
}
