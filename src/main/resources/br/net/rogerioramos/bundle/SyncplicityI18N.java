package br.net.rogerioramos.bundle;

import java.util.Locale;
import java.util.ResourceBundle;

public class SyncplicityI18N {

	public static final String LANG = "en";
	public static final String COUNTRY = "US";
	
	public static String getMessage(String key, String lang, String country, String... params) {
		ResourceBundle messages;
		Locale currentLocale = new Locale(lang, country);

		messages = ResourceBundle.getBundle("Messages", currentLocale);
		for (String p : params) {
			//TODO continue from here
		}
		
		return messages.getString(key);
	}
	
	public static String getMessage(String key, String lang, String country) {
		return getMessage(key, lang, country, "");
	}
	
	public static String getMessage(String key) {
		return getMessage(key, LANG, COUNTRY);
	}
}
